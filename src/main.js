import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import {store} from './store'
import firebase from 'firebase'
import {firebaseConfig} from './config'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import AOS from 'aos'
import 'aos/dist/aos.css';
import VeeValidate from 'vee-validate';

Vue.config.productionTip = false

Vue.use(Vuetify , {
  iconfont: 'mdi' // 'md' || 'mdi' || 'fa' || 'fa4'
})

Vue.use(VueAwesomeSwiper, /* { default global options } */)

Vue.use(VeeValidate ,{
  fieldsBagName: 'veeFields'
});

firebase.initializeApp(firebaseConfig)

.auth()
.onAuthStateChanged((firebaseUser) => {
  new Vue({
    el:'#app',
    router,
    store,
    vuetify: new Vuetify({
      icons: {
        iconfont: 'mdi'
      }
    }),
    created(){
      AOS.init();
      store.dispatch('autoSignIn',firebaseUser)
    },
    render: h => h(App),
  }).$mount('#app')
})
