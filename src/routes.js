import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

Vue.use(Router)

import Main from './views/Main.vue'
import Home from './views/Home.vue'

const routerOptions = [
    {
        path: '/',
        component: Home,
        name: 'name'
    },
    {
        path: '/main',
        component: Main,
        name: 'main',
        meta: { requiresAuth: true }
    }
]

const routes = routerOptions.map(route => {
    return {
        path: route.path,
        component: route.component,
        name: route.name,
        meta: route.meta
    }
})

const router = new Router({
    mode: 'history',
    routes: [
        ...routes,{
            path: '*',
            redirect: '/'
        }
    ]
})

router.beforeEach((to, from, next) => {
    const requiresAuth = to
    .matched
    .some(record => record.meta.requiresAuth)
    const user = firebase
    .auth()
    .currentUser
    if(requiresAuth && !user){
        next('/login')
    }
    next()
})

export default router