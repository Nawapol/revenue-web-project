import firebase from 'firebase'
import router from '../routes.js'
import axios from 'axios'
import Swal from 'sweetalert2'

export const actions = {
    userSignUp ({commit}, payload) {
        commit('setLoading', true)
        firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then( async firebaseUser => {
            await axios({
                method:'post',
                url:'localhost:3000/register',
                data:{
                    uid: firebaseUser.user.uid,
                }
            }).then(() => {
                commit('setUser', firebaseUser)
                commit('setLoading', false)
                Swal.fire(
                    'Register Success !',
                    'Your register success !.',
                    'success'
                    ).then(() => {
                        location.reload();
                    })
            }).catch((error) => {
                if(!error.response){
                    console.log('network error !');
                }else{
                    console.log(error.response.data.message);
                }
            })
        })
        .catch(error => {
            commit('setError', error.message)
            commit('setLoading', false)
        })
    },
    userSignIn({commit}, payload){
        commit('setLoading',true)
        firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(firebaseUser => {
            commit('setUser', firebaseUser)
            commit('setLoading', false)
            commit('setError', null)
            router.push({path: '/'})
        })
        .catch(error => {
            commit('setError',error.message)
            commit('setLoading', false)
        })
    },
    autoSignIn({commit}, payload){
        commit('setUser',payload)
    },
    userSignOut({commit}){
        firebase.auth().signOut()
        commit('setUser',null)
        router.push('/')
    }
}